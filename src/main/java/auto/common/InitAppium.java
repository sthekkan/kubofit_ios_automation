/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~ Author: Anil Kumar Lekkalapudi Email : sirianil@gmail.com Intro : This is a Java based mobile
 * automation framework built using Appium/TestNG.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * ~
 */

package auto.common;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import auto.utils.AutoUtilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class InitAppium {

    // The InitAppium class has the required Appium initialization

    public static AppiumDriver driver = null;

    @Parameters({ "pVer", "pName", "dName", "appLocation" })
    @BeforeSuite
    public static void setup(String pVer, String pName, String appLocation, String dName)
            throws IOException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformVersion", pVer);
        capabilities.setCapability("deviceName", dName);
        capabilities.setCapability("app", appLocation);

        System.out.println(pVer + ":" + pName + ":" + dName + ":" + appLocation);

        initialize(pName, capabilities);

        // This means that any search for elements on the page could take the time the implicit wait
        // is set for,
        // before throwing an exception that it cannot find the element on the page.
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

    }

    public static void initialize(String pfName, DesiredCapabilities capabilities)
            throws IOException {

        if (pfName.toUpperCase().matches("IOS")) {
            driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        } else if (pfName.toUpperCase().matches("ANDROID")) {
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        }
    }

    @AfterSuite
    public static void teardown() {
        driver.quit();
    }
}
